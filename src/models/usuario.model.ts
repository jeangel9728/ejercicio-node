import { Document, Schema, model } from 'mongoose';
// Mongoose.pluralize(null);

export interface UsuarioModel extends Document {
  /**
   * nombre del usuario
   */
  name: string;
  /**
   * edad del usuario
   */
  edad: number;
}

const usuarioSchema = new Schema({
  name: String,
  edad: Number
}, { collection: 'usuario' }
);

export default model<UsuarioModel>('usuario', usuarioSchema);
